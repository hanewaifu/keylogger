#ifndef KEYLOGGER_H
#define KEYLOGGER_H

#define RESET_VALUE 256

char *get_keyboard_event();

long keylogger(int keyboard, int out);

#endif
